class OptionalPasteSettings {
  get newPaste() {
    return $("#postform-text");
  }
  get pastSyntaxHighlighting() {
    return $("#select2-postform-format-container");
  }
  get pastExpirationSelect() {
    return $("#select2-postform-expiration-container");
  }
  get pasteNameTitle() {
    return $("#postform-name");
  }
  get createNewPaste() {
    return $("//button[text()='Create New Paste']");
  }

  getPastOptionsValue(option) {
    return $(`.select2-results__options li:nth-child(${option})`)
  }
  getPastOptionsNestedValue(option) {
    return $(`.select2-results__options--nested li:nth-child(${option})`)
  }

  async choosePastSyntaxHighlighting(option){
    await this.pastSyntaxHighlighting.click();
    await this.getPastOptionsNestedValue(option).click();
  }

  async choosePastExpirationOption(option) {
    await this.pastExpirationSelect.click();
    await this.getPastOptionsValue(option).click();
  }
}

module.exports = OptionalPasteSettings;
