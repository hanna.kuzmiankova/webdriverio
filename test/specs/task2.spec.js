const PastebinLandingPage = require("../../po/pages/pastebin.page");
const PastbinResultPage = require("../../po/pages/pastebin-result.page");

describe("WebDriver Task 2", () => {
  const optionalPasteSettings = PastebinLandingPage.optionalSettings;
  const gitCommand = `git config --global user.name "New Sheriff in Town
  git reset $(git commit-tree HEAD^{tree} -m "Legacy code
  git push origin master --force`;
  const pasteNameTitle = "how to gain dominance among developers";

  before(async () => {
    await PastebinLandingPage.open();
    await optionalPasteSettings.newPaste.setValue(gitCommand);
    await optionalPasteSettings.choosePastSyntaxHighlighting(1);
    await optionalPasteSettings.choosePastExpirationOption(3);
    await optionalPasteSettings.pasteNameTitle.setValue(pasteNameTitle);
    await optionalPasteSettings.createNewPaste.click();
    await PastbinResultPage.title.waitForDisplayed();
  });
  it("The browser page title should matche 'Paste Name/Title'", async () => {
    const title = await PastbinResultPage.title;
    await expect(title).toHaveText(pasteNameTitle);
  });
  it("Syntax should be for bash", async () => {
    const bashButton = await PastbinResultPage.syntaxHighlighting;
    await expect(bashButton).toBeExisting();
  });

  it("The code from pastebin-result.page should matche the code from pastebin.page", async () => {
    await PastbinResultPage.rawButton.click();
    await PastbinResultPage.valueOfcode.waitForDisplayed();
    const value = await PastbinResultPage.valueOfcode;
    await expect(value).toHaveText(gitCommand);
  });
});
