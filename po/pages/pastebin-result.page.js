class PastbinResultPage {
  get title() {
    return $(".info-top");
  }
  get syntaxHighlighting() {
    return $('a[href="/archive/bash"]');
  }
  get rawButton() {
    return $("a=raw");
  }
  get valueOfcode() {
    return $("pre");
  }
}
module.exports = new PastbinResultPage();
